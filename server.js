var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

  var bodyParser = require('body-parser')
  app.use(bodyParser.json())
var requestjson= require('request-json');

var path = require('path');

var urlmovimientosMlab ="https://api.mlab.com/api/1/databases/bdbanca2amr/collections/Movimientos?apiKey=1tUIsbrZXuqh3kNaayLTUeZ1QOZhc-o2";

var clienteMlab = requestjson.createClient(urlmovimientosMlab);



app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get("/",function(req,res){
  res.sendFile(path.join(__dirname,'index.html'));
})

app.get("/movimientos",function(req,res){
  clienteMlab.get('',function(err,resM,body){
  if(err){
    console.log(body);
  }  else{
    res.send(body);
  }
  })
})

/*var clienteNuevo={
  idcliente:3245,
  nombre:"Rubens",
  apellido:"Velozzz"
}*/

app.post("/crearCliente",function(req,res){
  clienteMlab.post('',req.body,function(err,resM,body){
  if(err){
    console.log(body);
  }  else{
    res.send(body);
  }
  })
})

app.get("/clientes/:idCliente",function(req,res){
  res.send("Aqui tiene el cliente numero:" + req.params.idCliente);
});

app.post("/", function(req,res){
  res.send("Hemos recibido su peticion actualizada");
})

app.put("/", function(req,res){
  res.send("Modificacion");
})

app.delete("/", function(req,res){
  res.send("Eliminar ");
})
